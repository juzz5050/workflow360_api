const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');


const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('combined'));

app.get('/status', (req,res) =>{
    res.send({
        msg: 'hello justin'
    })
})


app.listen(process.env.PORT || 8081)